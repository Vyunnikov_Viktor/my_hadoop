docker build -t renci/hadoop:2.9.0 ./2.9.0/
docker-compose -f 5-node-cluster.yml up -d
docker-compose -f 5-node-cluster.yml ps
./mapreduce-example.sh
docker-compose -f 5-node-cluster.yml stop && docker-compose -f 5-node-cluster.yml rm -f