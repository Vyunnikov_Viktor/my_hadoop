import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;

//import org.apache.hadoop.io.compress.SnappyCodec;

import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Counters;

import java.io.*;

import org.apache.hadoop.io.*;

import java.util.ArrayList;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordLength {


    private static class IpAddressValidator {

        private static final String zeroTo255
                = "([01]?[0-9]{1,2}|2[0-4][0-9]|25[0-5])";

        private static final String IP_REGEXP
                = zeroTo255 + "\\." + zeroTo255 + "\\."
                + zeroTo255 + "\\." + zeroTo255;

        private static final Pattern IP_PATTERN
                = Pattern.compile(IP_REGEXP);

        // Return true when *address* is IP Address
        private boolean isValid(String address) {
            return IP_PATTERN.matcher(address).matches();
        }
    }


    private static enum MalformedRowsCounter {
        MALFORMED_ROWS
    }


    public static class TokenizerMapper extends Mapper<Object, Text, Text, LengthWritable> {

        private static final String SEPARATOR = " - - ";

        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {


            String line = value.toString();


            int ipAddressEnd = line.indexOf(" ");

            if (ipAddressEnd == -1) {
                context.getCounter(MalformedRowsCounter.MALFORMED_ROWS).increment(1);
                return;
            }

            String ipAddresString = line.substring(0, ipAddressEnd);

            IpAddressValidator validator = new IpAddressValidator();


            if (!validator.isValid(ipAddresString)) {
                context.getCounter(MalformedRowsCounter.MALFORMED_ROWS).increment(1);
                return;
            }

            String separator = line.substring(ipAddressEnd, ipAddressEnd + SEPARATOR.length());

            if (!separator.equals(SEPARATOR)) {
                context.getCounter(MalformedRowsCounter.MALFORMED_ROWS).increment(1);
                return;
            }


            String content = line.substring(ipAddressEnd + SEPARATOR.length());


            context.write(new Text(ipAddresString), new LengthWritable(new DoubleWritable(content.length()), new IntWritable(1), new IntWritable(content.length())));
        }
    }

    public static class IntSumReducer extends Reducer<Text, LengthWritable, Text, LengthWritable> {

        public void reduce(Text key, Iterable<LengthWritable> values, Context context) throws IOException, InterruptedException {
            int sumLenght = 0;
            int requestCount = 0;
            for (LengthWritable val : values) {
                sumLenght += val.getSumLenght();
                requestCount += val.getRequestCount().get();
            }

            context.write(key, new LengthWritable(new DoubleWritable(((double) sumLenght) / requestCount), new IntWritable(requestCount), new IntWritable(sumLenght)));

        }
    }

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.set("mapred.textoutputformat.separatorText", ",");
        Job job = Job.getInstance(conf, "max word");
        job.setJarByClass(WordLength.class);
        job.setMapperClass(TokenizerMapper.class);
        job.setCombinerClass(IntSumReducer.class);
        job.setReducerClass(IntSumReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(LengthWritable.class);

//    job.setOutputFormatClass(TextOutputFormat.class);
//        job.setOutputFormatClass(SequenceFileOutputFormat.class);

//        job.setInputFormatClass(SequenceFileInputFormat.class);
//        job.setOutputFormatClass(TextOutputFormat.class);

//        FileOutputFormat.setCompressOutput(job, true);
//        FileOutputFormat.setOutputCompressorClass(job, SnappyCodec.class);
//        SequenceFileOutputFormat.setOutputCompressionType(job, SequenceFile.CompressionType.BLOCK);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        long startTime = System.nanoTime();
        boolean success = job.waitForCompletion(true);
        long endTime = System.nanoTime();
        long duration = (endTime - startTime);
        if (!success) {
            System.out.println("Job exited with error!!!");
        } else {
            System.out.println("Success!!! Duration: " + (duration / 1000000) + "ms");

            Counter recordNumCounter = job.getCounters().findCounter(MalformedRowsCounter.MALFORMED_ROWS);
            System.out.println("MalformedRowsCounter=[" + recordNumCounter.getValue() + "]");
        }
    }

    public static class LengthWritable implements WritableComparable<LengthWritable> {
        private IntWritable requestCount;
        private DoubleWritable avgLenght;
        private IntWritable sumLenght;

        public LengthWritable() {
            this.requestCount = new IntWritable();
            this.avgLenght = new DoubleWritable(0);
            this.sumLenght = new IntWritable(0);
        }

        public LengthWritable(DoubleWritable avgLenght, IntWritable requestCount, IntWritable sumLenght) {
            this.avgLenght = avgLenght;
            this.requestCount = requestCount;
            this.sumLenght = sumLenght;
        }

        public void set(DoubleWritable length, IntWritable requestCount, IntWritable sumLenght) {
            this.avgLenght = length;
            this.requestCount = requestCount;
            this.sumLenght = sumLenght;
        }

        public void set(LengthWritable o) {
            this.avgLenght = new DoubleWritable(o.avgLenght.get());
            this.requestCount = new IntWritable(o.requestCount.get());
            this.sumLenght = new IntWritable(o.sumLenght.get());
        }

        public IntWritable getRequestCount() {
            return new IntWritable(requestCount.get());
        }

        public double getAvgLenght() {
            return avgLenght.get();
        }

        public int getSumLenght() {
            return sumLenght.get();
        }

        @Override
        public void readFields(DataInput in) throws IOException {
            avgLenght.readFields(in);
            requestCount.readFields(in);
            sumLenght.readFields(in);
        }

        @Override
        public void write(DataOutput out) throws IOException {
            avgLenght.write(out);
            requestCount.write(out);
            sumLenght.write(out);
        }

        @Override
        public int compareTo(LengthWritable o) {
            return avgLenght.compareTo(o.avgLenght);
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof LengthWritable) {
                LengthWritable other = (LengthWritable) o;
                return avgLenght.equals(other.avgLenght);
            }
            return false;
        }

        @Override
        public int hashCode() {
            return avgLenght.hashCode();
        }

        @Override
        public String toString() {
            return "Request count: " + requestCount.toString() + " | avgLenght: " + avgLenght.toString() + " | sumLenght: " + sumLenght.toString();
        }
    }
}
